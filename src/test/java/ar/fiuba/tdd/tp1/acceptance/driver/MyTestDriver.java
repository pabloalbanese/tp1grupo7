package ar.fiuba.tdd.tp1.acceptance.driver;

import ar.fiuba.tdd.tp1.api.ConcreteWorkbookControllerBuilder;
import ar.fiuba.tdd.tp1.api.DuplicateBookNameException;
import ar.fiuba.tdd.tp1.api.DuplicateSheetNameException;
import ar.fiuba.tdd.tp1.api.ISheetCell;
import ar.fiuba.tdd.tp1.api.ISpreadsheet;
import ar.fiuba.tdd.tp1.api.IWorkbook;
import ar.fiuba.tdd.tp1.api.IWorkbookController;
import ar.fiuba.tdd.tp1.api.IWorkbookControllerBuilder;
import ar.fiuba.tdd.tp1.api.InvalidBookNameException;
import ar.fiuba.tdd.tp1.api.InvalidCellIDException;
import ar.fiuba.tdd.tp1.api.InvalidFormulaException;
import ar.fiuba.tdd.tp1.api.InvalidParseStateException;
import ar.fiuba.tdd.tp1.api.InvalidSheetNameException;
import ar.fiuba.tdd.tp1.api.UnrecognizedTokenException;
import ar.fiuba.tdd.tp1.workbook.DetectCycle;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MyTestDriver implements SpreadSheetTestDriver {

    private IWorkbookController        controller;
    private IWorkbookControllerBuilder builder = new ConcreteWorkbookControllerBuilder();

    public MyTestDriver() {
        controller = builder.buildWorkbookController();
    }

    private ISheetCell getCell(String workBookName, String workSheetName, String cellId) {
        ISheetCell cell = null;
        try {
            cell = controller.getBook(workBookName).getSheet(workSheetName, true).getCell(cellId, true);
        } catch (InvalidCellIDException e1) {
            throw new BadReferenceException();
        } catch (InvalidSheetNameException e1) {
            throw new UndeclaredWorkSheetException();
        } catch (InvalidBookNameException e1) {
            throw new UndeclaredWorkSheetException();
        }
        return cell;
    }

    @Override
    public List<String> workBooksNames() {
        List<String> names = new ArrayList<String>();
        for (IWorkbook book : controller.getBooks()) {
            names.add(book.getName());
        }
        return names;
    }

    @Override
    public void createNewWorkBookNamed(String name) {

        try {
            controller.addBook(name);
        } catch (DuplicateBookNameException ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public void createNewWorkSheetNamed(String workbookName, String name) {

        try {
            controller.getBook(workbookName).addSheet(name);
        } catch (DuplicateSheetNameException ex) {
            ex.printStackTrace();
        } catch (InvalidBookNameException ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public List<String> workSheetNamesFor(String workBookName) {

        List<String> names = new ArrayList<String>();
        try {
            IWorkbook book = controller.getBook(workBookName);

            for (ISpreadsheet sheet : book.getSheets()) {
                names.add(sheet.getName());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return names;
    }

    @Override
    public void setCellValue(String workBookName, String workSheetName, String cellId, String value) {

        ISheetCell cell = getCell(workBookName, workSheetName, cellId);
        try {
            cell.setValue(value);
        } catch (InvalidParseStateException ex) {
            throw new BadFormulaException();
        } catch (UnrecognizedTokenException ex) {
            try {
                cell.setValue("Error:BAD_FORMULA");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public String getCellValueAsString(String workBookName, String workSheetName, String cellId) {
        ISheetCell cell = getCell(workBookName, workSheetName, cellId);
        String value = null;
        if (cell.getType() == "String") {
            return cell.getRawValue();
        } else {
            try {
                if ( cell.getFormatter().getType().equals("NULL") == false ) {
                    value = cell.getFormattedResult();
                } else {
                    value = cell.getResult();
                }
            } catch (InvalidFormulaException | InvalidSheetNameException | InvalidBookNameException e) {
                e.printStackTrace();
            }
        }
        return value;
    }

    @Override
    public double getCellValueAsDouble(String workBookName, String workSheetName, String cellId) {

        ISheetCell cell = getCell(workBookName, workSheetName, cellId);
        String value = null;
        DetectCycle cycleDetector = new DetectCycle();

        try {
            ISpreadsheet sheet = controller.getBook(workBookName).getSheet(workSheetName, true);
            cycleDetector.addCells(sheet.getCells());
            if (cycleDetector.hasCycle(cellId)) {
                throw new BadReferenceException();
            } else {
                if (cell.getType() == "String") {
                    throw new BadFormatException();
                } else {
                    value = cell.getResult();
                    if (value.equals("Error:BAD_FORMULA")) {
                        throw new BadFormulaException();
                    }
                }
                return Double.valueOf(value);
            }
        } catch (InvalidFormulaException | NumberFormatException ex) {
            throw new BadFormulaException();
        } catch (InvalidSheetNameException | InvalidBookNameException ex) {
            throw new BadReferenceException();
        }
    }

    @Override
    public void undo() {
        try {
            controller.undo();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void redo() {
        try {
            controller.redo();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void setCellFormatter(String workBookName, String workSheetName, String cellId, String formatter,
            String format) {
        ISheetCell cell = getCell(workBookName, workSheetName, cellId);

        cell.getFormatter().setNewFormat(formatter, format);
    }

    @Override
    public void setCellType(String workBookName, String workSheetName, String cellId, String type) {
        ISheetCell cell = getCell(workBookName, workSheetName, cellId);
        cell.setType(type);
    }

    @Override
    public void persistWorkBook(String workBookName, String fileName) {
        try {
            controller.exportBookToJson(workBookName, fileName);
        } catch (InvalidBookNameException | IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void reloadPersistedWorkBook(String fileName) {
        try {
            controller.importBookFromJson(fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void saveAsCSV(String workBookName, String sheetName, String path) {
        try {
            controller.saveSheetAsCSV(workBookName, sheetName, path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        
    }

    @Override
    public void loadFromCSV(String workBookName, String sheetName, String path) {
        try {
            controller.loadSheetFromCSV(workBookName, sheetName, path);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int sheetCountFor(String workBookName) {
        int sheetCount = 0;
        try {
            IWorkbook book = controller.getBook(workBookName);

            for (ISpreadsheet sheet : book.getSheets()) {
                sheetCount++;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return sheetCount;
    }

    @Override
    public void createRange(String name, String value) {
        controller.createRange(name, value);
    }

}
