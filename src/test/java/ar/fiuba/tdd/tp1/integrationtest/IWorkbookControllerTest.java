package ar.fiuba.tdd.tp1.integrationtest;

import ar.fiuba.tdd.tp1.api.ConcreteWorkbookControllerBuilder;
import ar.fiuba.tdd.tp1.api.DuplicateBookNameException;
import ar.fiuba.tdd.tp1.api.IWorkbookController;
import ar.fiuba.tdd.tp1.api.InvalidBookNameException;

import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class IWorkbookControllerTest {

    ConcreteWorkbookControllerBuilder builder    = new ConcreteWorkbookControllerBuilder();
    IWorkbookController               controller = builder.buildWorkbookController();

    @Test
    public void createWorkbookController() {
        controller = builder.buildWorkbookController();
        assertTrue(controller != null);
    }

    @Test
    public void addBook() {
        controller = builder.buildWorkbookController();
        try {
            controller.addBook("book1");
        } catch (DuplicateBookNameException e) {
            fail();
        }
    }

    @Test(expected = DuplicateBookNameException.class)
    public void addDuplicateBook() throws DuplicateBookNameException {
        controller = builder.buildWorkbookController();
        controller.addBook("book1");
        controller.addBook("book1");
    }

    @Test
    public void removeBook() {
        controller = builder.buildWorkbookController();
        try {
            controller.addBook("book1");
            controller.removeBook("book1");
        } catch (InvalidBookNameException e) {
            fail();
        } catch (DuplicateBookNameException e) {
            fail();
        }
    }

    @Test(expected = InvalidBookNameException.class)
    public void removeNonexistingBook() throws InvalidBookNameException {
        controller = builder.buildWorkbookController();
        controller.removeBook("book1");
    }

    @Test
    public void getBook() {
        controller = builder.buildWorkbookController();
        try {
            controller.addBook("book1");
            controller.getBook("book1");
        } catch (DuplicateBookNameException e) {
            fail();
        } catch (InvalidBookNameException e) {
            fail();
        }
    }

    @Test(expected = InvalidBookNameException.class)
    public void getNonexistingBook() throws InvalidBookNameException {
        controller = builder.buildWorkbookController();
        controller.getBook("book2");
    }

}
