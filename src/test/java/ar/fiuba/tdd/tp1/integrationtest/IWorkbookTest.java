package ar.fiuba.tdd.tp1.integrationtest;

import ar.fiuba.tdd.tp1.api.ConcreteWorkbookControllerBuilder;
import ar.fiuba.tdd.tp1.api.DuplicateBookNameException;
import ar.fiuba.tdd.tp1.api.DuplicateSheetNameException;
import ar.fiuba.tdd.tp1.api.IWorkbook;
import ar.fiuba.tdd.tp1.api.IWorkbookController;
import ar.fiuba.tdd.tp1.api.InvalidBookNameException;
import ar.fiuba.tdd.tp1.api.InvalidSheetNameException;

import org.junit.Test;

import static org.junit.Assert.fail;

public class IWorkbookTest {

    private IWorkbook createBook() {

        ConcreteWorkbookControllerBuilder builder = new ConcreteWorkbookControllerBuilder();
        IWorkbookController controller = builder.buildWorkbookController();
        try {
            controller.addBook("book1");
            return controller.getBook("book1");
        } catch (DuplicateBookNameException e) {
            e.printStackTrace();
        } catch (InvalidBookNameException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Test
    public void addSheet() {
        IWorkbook book = createBook();
        try {
            book.addSheet("sheet1");
        } catch (DuplicateSheetNameException e) {
            fail();
        }
    }

    @Test(expected = DuplicateSheetNameException.class)
    public void addDuplicateSheet() throws DuplicateSheetNameException {
        IWorkbook book = createBook();

        book.addSheet("sheet1");
        book.addSheet("sheet1");
    }

    @Test
    public void removeSheet() {
        IWorkbook book = createBook();
        try {
            book.addSheet("sheet1");
            book.removeSheet("sheet1");
        } catch (InvalidSheetNameException e) {
            fail();
        } catch (DuplicateSheetNameException e) {
            fail();
        }
    }

    @Test(expected = InvalidSheetNameException.class)
    public void removeNonexistingBook() throws InvalidSheetNameException {
        IWorkbook book = createBook();
        book.removeSheet("sheet1");
    }

    @Test
    public void getBook() {
        IWorkbook book = createBook();
        try {
            book.addSheet("sheet1");
            book.getSheet("sheet1", true);
        } catch (DuplicateSheetNameException e) {
            fail();
        } catch (InvalidSheetNameException e) {
            fail();
        }
    }

    @Test(expected = InvalidSheetNameException.class)
    public void getNonexistingBook() throws InvalidSheetNameException {
        IWorkbook book = createBook();
        book.getSheet("sheet2", true);
    }

}
