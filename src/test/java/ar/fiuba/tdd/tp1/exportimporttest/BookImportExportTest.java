package ar.fiuba.tdd.tp1.exportimporttest;

import ar.fiuba.tdd.tp1.api.ConcreteWorkbookControllerBuilder;
import ar.fiuba.tdd.tp1.api.ISpreadsheet;
import ar.fiuba.tdd.tp1.api.IWorkbook;
import ar.fiuba.tdd.tp1.api.IWorkbookController;
import ar.fiuba.tdd.tp1.api.IWorkbookControllerBuilder;
import ar.fiuba.tdd.tp1.api.InvalidBookNameException;
import ar.fiuba.tdd.tp1.api.InvalidCellIDException;
import ar.fiuba.tdd.tp1.api.InvalidSheetNameException;
import ar.fiuba.tdd.tp1.importexport.IJsonable;
import ar.fiuba.tdd.tp1.importexport.JsonBook;
import ar.fiuba.tdd.tp1.workbook.NumberFormatter;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class BookImportExportTest {

    private IWorkbookController        controller;
    private IWorkbookControllerBuilder builder = new ConcreteWorkbookControllerBuilder();
    
    private String bookName = "matori.jss";
    private String sheetName = "robertito";
    
    private IWorkbook newTestBook() {
        
        ISpreadsheet sheet = null;
        IWorkbook book = null;
        try {
            
            controller.addBook(bookName);
            controller.getBook(bookName).addSheet(sheetName);
            
            sheet = controller.getBook(bookName).getSheet(sheetName, true);
            
            sheet.getCell("A1", true).setValue("1991.19");
            sheet.getCell("A2", true).setValue("19-10-2015");
            sheet.getCell("B1", true).setValue("Holanda");
            sheet.getCell("C3", true).setValue("= A1+A2");
            sheet.getCell("B1", true).setFormatter(new NumberFormatter());
            
            book = controller.getBook(bookName);
            
        } catch (InvalidSheetNameException e1) {
            //e1.printStackTrace();
        } catch (InvalidBookNameException e1) {
            //e1.printStackTrace();
        } catch (InvalidCellIDException e) {
            //e.printStackTrace();
        } catch (Exception e) {
            //e.printStackTrace();
        }
        
        return book;
    }
    
    @Before
    public void setUp() {
        controller = builder.buildWorkbookController();
    }
    
    @Test
    public void exportAndImportBookJson() {
        IWorkbook book = newTestBook();
        
        IJsonable jsonbookForExport = new JsonBook(book);
        String exported = jsonbookForExport.toJson();
        
        IJsonable jsonbookForImport = new JsonBook();
        jsonbookForImport.fromJson(exported);
        
        String imported = jsonbookForImport.toJson();
        
        assertTrue(exported.equals(imported));
    }
    
    @Test
    public void exportBookToCSV() {
        
    }
    
    @Test
    public void importBookFromCSV() {
        
    }
}
