package ar.fiuba.tdd.tp1.exportimporttest;

import ar.fiuba.tdd.tp1.api.IFormatter;
import ar.fiuba.tdd.tp1.api.ISheetCell;
import ar.fiuba.tdd.tp1.importexport.IJsonable;
import ar.fiuba.tdd.tp1.importexport.JsonCell;
import ar.fiuba.tdd.tp1.workbook.ConcreteCell;
import ar.fiuba.tdd.tp1.workbook.DateFormatter;
import ar.fiuba.tdd.tp1.workbook.MoneyFormatter;
import ar.fiuba.tdd.tp1.workbook.NullFormatter;
import ar.fiuba.tdd.tp1.workbook.NumberFormatter;
import ar.fiuba.tdd.tp1.workbook.StringFormatter;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class CellImportExportTest {

    private String infoJson = "";

    private enum Formats {
        DATE, MONEY, NUMBER, STRING, NULL
    }

    private void fillInfoJson(ISheetCell cell) {
        infoJson += 
            "{\n"
            + "  \"sheet\": \"default\",\n"
            + "  \"id\": \"" + cell.getId() + "\",\n"
            + "  \"value\": \"" + cell.getRawValue() + "\",\n"
            + "  \"type\": \"" + cell.getFormatter().getType() + "\",\n"
            + "  \"formatter\": {\n"
            + "    " + cell.getFormatter().toString() + "\n"
            + "  }\n"
            + "}";
    }

    private IFormatter getFormatter(Formats format) {
        switch (format) {
            case DATE: return new DateFormatter();
            case MONEY: return new MoneyFormatter();
            case NUMBER: return new NumberFormatter();
            case STRING: return new StringFormatter();
            case NULL: return new StringFormatter();
            default: return new NullFormatter();
        }
    }
    
    private ISheetCell newCellWithFormat(Formats format) {
        ISheetCell cell = new ConcreteCell("A1");

        try {
            cell.setValue("= 1234");
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        cell.setFormatter(getFormatter(format));

        return cell;
    }

    @Before
    public void setUp() {
        infoJson = "";
    }

    @Test
    public void exportDateCellToJson() {
        ISheetCell dateCell = newCellWithFormat(Formats.DATE);

        IJsonable jsoncell = new JsonCell(dateCell);

        fillInfoJson(dateCell);

        // System.out.println(jsoncell.toJson());

        assertTrue(jsoncell.toJson().equals(infoJson));
    }

    @Test
    public void exportMoneyCellToJson() {
        ISheetCell moneyCell = newCellWithFormat(Formats.MONEY);

        IJsonable jsoncell = new JsonCell(moneyCell);

        fillInfoJson(moneyCell);

        // System.out.println(jsoncell.toJson());

        assertTrue(jsoncell.toJson().equals(infoJson));
    }

    @Test
    public void exportNumberCellToJson() {
        ISheetCell numberCell = newCellWithFormat(Formats.NUMBER);

        IJsonable jsoncell = new JsonCell(numberCell);

        fillInfoJson(numberCell);

        // System.out.println(jsoncell.toJson());

        assertTrue(jsoncell.toJson().equals(infoJson));
    }

    @Test
    public void exportStringCellToJson() {
        ISheetCell stringCell = newCellWithFormat(Formats.STRING);

        IJsonable jsoncell = new JsonCell(stringCell);

        fillInfoJson(stringCell);

        // System.out.println(jsoncell.toJson());

        assertTrue(jsoncell.toJson().equals(infoJson));
    }
    
    @Test
    public void exportCellWithNullFormatToJson() {
        ISheetCell cellWithNullFormat = newCellWithFormat(Formats.NULL);

        IJsonable jsoncell = new JsonCell(cellWithNullFormat);

        fillInfoJson(cellWithNullFormat);

        // System.out.println(jsoncell.toJson());

        assertTrue(jsoncell.toJson().equals(infoJson));
    }

    @Test
    public void importDateCellFromJson() {
        JsonCell jsoncell = new JsonCell();
        ISheetCell dateCell = newCellWithFormat(Formats.DATE);

        fillInfoJson(dateCell);

        jsoncell.fromJson(infoJson);

        // System.out.println(jsoncell.toJson());

        assertTrue(jsoncell.toJson().toString().equals(infoJson));
    }

    @Test
    public void importNumberCellFromJson() {
        JsonCell jsoncell = new JsonCell();
        ISheetCell numberCell = newCellWithFormat(Formats.NUMBER);

        fillInfoJson(numberCell);

        jsoncell.fromJson(infoJson);

        // System.out.println(jsoncell.toJson());

        assertTrue(jsoncell.toJson().toString().equals(infoJson));
    }

    @Test
    public void importMoneyCellFromJson() {
        JsonCell jsoncell = new JsonCell();
        ISheetCell moneyCell = newCellWithFormat(Formats.MONEY);

        fillInfoJson(moneyCell);

        jsoncell.fromJson(infoJson);

        // System.out.println(jsoncell.toJson());

        assertTrue(jsoncell.toJson().toString().equals(infoJson));
    }

    @Test
    public void importStringCellFromJson() {
        JsonCell jsoncell = new JsonCell();
        ISheetCell stringCell = newCellWithFormat(Formats.STRING);

        fillInfoJson(stringCell);

        jsoncell.fromJson(infoJson);

        // System.out.println(jsoncell.toJson());

        assertTrue(jsoncell.toJson().toString().equals(infoJson));
    }
    
    @Test
    public void importCellWithNullFormatFromJson() {
        JsonCell jsoncell = new JsonCell();
        ISheetCell cellWithNullFormat = newCellWithFormat(Formats.NULL);

        fillInfoJson(cellWithNullFormat);

        jsoncell.fromJson(infoJson);

        // System.out.println(jsoncell.toJson());

        assertTrue(jsoncell.toJson().toString().equals(infoJson));
    }

    @Test
    public void exportCellToCSV() {

    }

    @Test
    public void importCellFromCSV() {

    }
}
