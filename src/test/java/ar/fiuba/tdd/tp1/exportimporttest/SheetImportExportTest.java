package ar.fiuba.tdd.tp1.exportimporttest;

import ar.fiuba.tdd.tp1.api.ConcreteWorkbookControllerBuilder;
import ar.fiuba.tdd.tp1.api.ISpreadsheet;
import ar.fiuba.tdd.tp1.api.IWorkbookController;
import ar.fiuba.tdd.tp1.api.IWorkbookControllerBuilder;
import ar.fiuba.tdd.tp1.api.InvalidBookNameException;
import ar.fiuba.tdd.tp1.api.InvalidCellIDException;
import ar.fiuba.tdd.tp1.api.InvalidSheetNameException;
import ar.fiuba.tdd.tp1.importexport.IJsonable;
import ar.fiuba.tdd.tp1.importexport.JsonSheet;
import ar.fiuba.tdd.tp1.workbook.NumberFormatter;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class SheetImportExportTest {

    private IWorkbookController        controller;
    private IWorkbookControllerBuilder builder = new ConcreteWorkbookControllerBuilder();
    
    private String bookName = "matori.jss";
    private String sheetName = "robertito";
    
    private ISpreadsheet newTestSheet() {
        
        ISpreadsheet sheet = null;
        try {
            
            controller.addBook(bookName);
            controller.getBook(bookName).addSheet(sheetName);
            
            sheet = controller.getBook(bookName).getSheet(sheetName, true);
            
            sheet.getCell("A1", true).setValue("1991.19");
            sheet.getCell("A2", true).setValue("19-10-2015");
            sheet.getCell("B1", true).setValue("Holanda");
            sheet.getCell("B1", true).setFormatter(new NumberFormatter());
            
        } catch (InvalidSheetNameException e1) {
            e1.printStackTrace();
        } catch (InvalidBookNameException e1) {
            e1.printStackTrace();
        } catch (InvalidCellIDException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return sheet;
    }
    
    @Before
    public void setUp() {
        controller = builder.buildWorkbookController();
    }
    
    @Test
    public void exportAndImportSheetJson() throws InvalidCellIDException, Exception {
        ISpreadsheet sheet = newTestSheet();
        
        IJsonable jsonsheetForExport = new JsonSheet(sheet);
        String exported = jsonsheetForExport.toJson();
        
        IJsonable jsonsheetForImport = new JsonSheet();
        jsonsheetForImport.fromJson(exported);
        
        String imported = jsonsheetForImport.toJson();
        
        assertTrue(exported.equals(imported));
    }
}
