package ar.fiuba.tdd.tp1.undoredotest;

import ar.fiuba.tdd.tp1.api.ISheetCell;
import ar.fiuba.tdd.tp1.undoredo.CellChange;
import ar.fiuba.tdd.tp1.undoredo.IUndoable;
import ar.fiuba.tdd.tp1.undoredo.UndoRedo;
import ar.fiuba.tdd.tp1.workbook.ConcreteCell;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertTrue;

public class UndoRedoTest {

    @Rule
    public ExpectedException thrown   = ExpectedException.none();

    private UndoRedo         undoredo = new UndoRedo();
    private ISheetCell       cell     = new ConcreteCell("A1");
    String                   value1   = "=20";
    String                   value2   = "=30";
    String                   value3   = "=40";
    String                   value4   = "=50";
    String                   value5   = "=60";

    @Test
    public void singleUndoCell() throws Exception {
        cell.setValue(value1);

        IUndoable change = new CellChange(cell, cell.getRawValue(), value2);
        undoredo.pushChange(change);
        cell.setValue(value2);

        assertTrue(cell.getRawValue() == value2);
        undoredo.undo();
        assertTrue(cell.getRawValue() == value1);
    }

    @Test
    public void doubleUndoCell() throws Exception {
        cell.setValue(value1);

        IUndoable change = new CellChange(cell, cell.getRawValue(), value2);
        undoredo.pushChange(change);
        cell.setValue(value2);

        IUndoable change2 = new CellChange(cell, cell.getRawValue(), value3);
        undoredo.pushChange(change2);
        cell.setValue(value3);

        IUndoable change3 = new CellChange(cell, cell.getRawValue(), value4);
        undoredo.pushChange(change3);
        cell.setValue(value4);

        undoredo.undo();
        undoredo.undo();

        assertTrue(cell.getRawValue() == value2);
    }

    @Test
    public void multipleUndoCell() throws Exception {
        cell.setValue(value1);

        IUndoable change = new CellChange(cell, cell.getRawValue(), value2);
        undoredo.pushChange(change);
        cell.setValue(value2);

        IUndoable change2 = new CellChange(cell, cell.getRawValue(), value3);
        undoredo.pushChange(change2);
        cell.setValue(value3);

        IUndoable change3 = new CellChange(cell, cell.getRawValue(), value4);
        undoredo.pushChange(change3);
        cell.setValue(value4);

        IUndoable change4 = new CellChange(cell, cell.getRawValue(), value5);
        undoredo.pushChange(change4);
        cell.setValue(value5);

        undoredo.undo();
        undoredo.undo();
        undoredo.undo();

        assertTrue(cell.getRawValue() == value2);
    }

    @Test
    public void singleUndoEmptyCellException() throws Exception {
        cell.setValue(value1);

        IUndoable change = new CellChange(cell, cell.getRawValue(), value2);
        undoredo.pushChange(change);
        cell.setValue(value2);

        undoredo.undo();

        thrown.expectMessage("There are no more expressions to undo");
        undoredo.undo();
    }

    @Test
    public void multipleRedoCell() throws Exception {
        cell.setValue(value1);

        IUndoable change = new CellChange(cell, cell.getRawValue(), value2);
        undoredo.pushChange(change);
        cell.setValue(value2);

        IUndoable change2 = new CellChange(cell, cell.getRawValue(), value3);
        undoredo.pushChange(change2);
        cell.setValue(value3);

        assertTrue(cell.getRawValue() == value3);

        undoredo.undo();
        undoredo.undo();

        assertTrue(cell.getRawValue() == value1);

        undoredo.redo();
        undoredo.redo();

        assertTrue(cell.getRawValue() == value3);

    }

    @Test
    public void singleRedoCell() throws Exception {
        cell.setValue(value1);

        IUndoable change = new CellChange(cell, cell.getRawValue(), value2);
        undoredo.pushChange(change);
        cell.setValue(value2);

        assertTrue(cell.getRawValue() == value2);

        undoredo.undo();

        assertTrue(cell.getRawValue() == value1);

        undoredo.redo();

        assertTrue(cell.getRawValue() == value2);
    }

    @Test
    public void singleRedoEmptyCellException() throws Exception {
        cell.setValue(value1);

        IUndoable change = new CellChange(cell, cell.getRawValue(), value2);
        undoredo.pushChange(change);
        cell.setValue(value2);

        undoredo.undo();

        undoredo.redo();

        thrown.expectMessage("There are no more expressions to redo");
        undoredo.redo();
    }
}