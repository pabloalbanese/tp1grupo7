package ar.fiuba.tdd.tp1.expressioninterpretertest;

import ar.fiuba.tdd.tp1.api.InvalidParseStateException;
import ar.fiuba.tdd.tp1.api.UnrecognizedTokenException;
import ar.fiuba.tdd.tp1.expressioninterpreter.ExpressionInterpreter;
import ar.fiuba.tdd.tp1.parser.ParseTree;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class ExpressionInterpreterTest {

    @Test
    public void parseString() throws Exception {
        ParseTree tree = ExpressionInterpreter.getInstance().parse("aString");
        String result = ExpressionInterpreter.getInstance().interpret(tree);
        assertTrue(result.equals("aString"));
    }

    @Test(expected = UnrecognizedTokenException.class)
    public void parseStringAfterEqual() throws Exception {
        ParseTree tree = ExpressionInterpreter.getInstance().parse("=aString");
        String result = ExpressionInterpreter.getInstance().interpret(tree);
        assertTrue(result.equals("aString"));
    }

    @Test
    public void parseEqualWithNoFollowup() throws Exception {
        ParseTree tree = ExpressionInterpreter.getInstance().parse("=");
        String result = ExpressionInterpreter.getInstance().interpret(tree);
        assertTrue(result.equals("="));
    }

    @Test
    public void parseEqualInteger() throws Exception {
        ParseTree tree = ExpressionInterpreter.getInstance().parse("=2");
        String result = ExpressionInterpreter.getInstance().interpret(tree);
        assertTrue(result.equals("2"));
    }

    @Test
    public void parseEqualFloat() throws Exception {
        ParseTree tree = ExpressionInterpreter.getInstance().parse("=2.0");
        String result = ExpressionInterpreter.getInstance().interpret(tree);
        assertTrue(result.equals("2.0"));
    }

    @Test
    public void parseEqualSum() throws Exception {
        ParseTree tree = ExpressionInterpreter.getInstance().parse("=2+1");
        String result = ExpressionInterpreter.getInstance().interpret(tree);
        assertTrue(result.equals("3.0"));
    }

    @Test
    public void parseEqualMinus() throws Exception {
        ParseTree tree = ExpressionInterpreter.getInstance().parse("=2-1");
        String result = ExpressionInterpreter.getInstance().interpret(tree);
        assertTrue(result.equals("1.0"));
    }

    @Test
    public void parseEqualTimes() throws Exception {
        ParseTree tree = ExpressionInterpreter.getInstance().parse("=2*3");
        String result = ExpressionInterpreter.getInstance().interpret(tree);
        assertTrue(result.equals("6.0"));
    }

    @Test
    public void parseEqualDivide() throws Exception {
        ParseTree tree = ExpressionInterpreter.getInstance().parse("=8/2");
        String result = ExpressionInterpreter.getInstance().interpret(tree);
        assertTrue(result.equals("4.0"));
    }

    @Test
    public void parseComplexExpression() throws Exception {
        ParseTree tree = ExpressionInterpreter.getInstance().parse("=1+3*4-2");
        String result = ExpressionInterpreter.getInstance().interpret(tree);
        assertTrue(result.equals("11.0"));
    }

    @Test
    public void parseOtherComplexExpression() throws Exception {
        ParseTree tree = ExpressionInterpreter.getInstance().parse("=1+3*4-24/4");
        String result = ExpressionInterpreter.getInstance().interpret(tree);
        assertTrue(result.equals("7.0"));
    }

    @Test
    public void parseExpressionWithParentheses() throws Exception {
        ParseTree tree = ExpressionInterpreter.getInstance().parse("=1+3*(4-2)");
        String result = ExpressionInterpreter.getInstance().interpret(tree);
        assertTrue(result.equals("7.0"));
    }

    @Test
    public void parseExpressionWithNestedParentheses() throws Exception {
        ParseTree tree = ExpressionInterpreter.getInstance().parse("=1+3*(7/2-(4-2))");
        String result = ExpressionInterpreter.getInstance().interpret(tree);
        assertTrue(result.equals("5.5"));
    }

    @Test
    public void parseExpressionWithUnaryPlus() throws Exception {
        ParseTree tree = ExpressionInterpreter.getInstance().parse("=+1");
        String result = ExpressionInterpreter.getInstance().interpret(tree);
        assertTrue(result.equals("1.0"));
    }

    @Test
    public void parseExpressionWithUnaryMinus() throws Exception {
        ParseTree tree = ExpressionInterpreter.getInstance().parse("=-(2-3)*(-2)");
        String result = ExpressionInterpreter.getInstance().interpret(tree);
        assertTrue(result.equals("-2.0"));
    }

    @Test(expected = UnrecognizedTokenException.class)
    public void parseInvalidExpression1() throws Exception {
        ExpressionInterpreter.getInstance().parse("=+a1");
    }

    @Test(expected = InvalidParseStateException.class)
    public void parseInvalidExpression2() throws Exception {
        ExpressionInterpreter.getInstance().parse("=+*1");
    }

    @Test
    public void parseExpressionWithSpaces() throws Exception {
        ParseTree tree = ExpressionInterpreter.getInstance().parse("= 1 - 0 - 3.5 - -1");
        String result = ExpressionInterpreter.getInstance().interpret(tree);
        assertTrue(result.equals("-1.5"));
    }

}
