package ar.fiuba.tdd.tp1.expressioninterpreter;

import ar.fiuba.tdd.tp1.api.ISheetCell;
import ar.fiuba.tdd.tp1.api.InvalidBookNameException;
import ar.fiuba.tdd.tp1.api.InvalidFormulaException;
import ar.fiuba.tdd.tp1.api.InvalidSheetNameException;

import java.util.List;

public class ReturnAverageAction extends ReturnOpOnRangeAction {

    @Override
    protected String opOnRange(List<ISheetCell> cells)
            throws NumberFormatException, InvalidFormulaException, InvalidSheetNameException, InvalidBookNameException {
        float total = 0;

        for (ISheetCell cell : cells) {
            total += Float.valueOf(cell.getResult());
        }
        return Float.toString(total / cells.size());
    }

}
