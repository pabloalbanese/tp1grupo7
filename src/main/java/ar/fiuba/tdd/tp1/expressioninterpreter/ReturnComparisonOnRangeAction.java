package ar.fiuba.tdd.tp1.expressioninterpreter;

import ar.fiuba.tdd.tp1.api.ISheetCell;
import ar.fiuba.tdd.tp1.api.InvalidBookNameException;
import ar.fiuba.tdd.tp1.api.InvalidFormulaException;
import ar.fiuba.tdd.tp1.api.InvalidSheetNameException;

import java.util.List;

public abstract class ReturnComparisonOnRangeAction extends ReturnOpOnRangeAction {

    @Override
    protected String opOnRange(List<ISheetCell> cells)
            throws NumberFormatException, InvalidFormulaException, InvalidSheetNameException, InvalidBookNameException {
        boolean resultValueFound = false;
        float currentResultValue = 0;

        for (ISheetCell cell : cells) {
            if ((resultValueFound && betterMatch(Float.valueOf(cell.getResult()), currentResultValue))
                    || (!resultValueFound)) {
                resultValueFound = true;
                currentResultValue = Float.valueOf(cell.getResult());
            }
        }
        return Float.toString(currentResultValue);
    }

    protected abstract boolean betterMatch(float value, float currentResultValue);

}
