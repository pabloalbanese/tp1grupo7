package ar.fiuba.tdd.tp1.expressioninterpreter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

// Describes the grammar accepted by cells in a spreadsheet.
public class CellGrammar {

    private CellGrammar() {
    }

    @SuppressWarnings("CPD-START")
    public static final String                                EQUAL           = "equal";
    public static final String                                NUM_VALUE       = "numValue";
    public static final String                                REF_VALUE       = "refValue";
    public static final String                                PLUS            = "plus";
    public static final String                                MINUS           = "minus";
    public static final String                                TIMES           = "times";
    public static final String                                DIVIDE          = "divide";
    public static final String                                EXPONENTIAL     = "exponential";
    public static final String                                OPEN_PAR        = "openParentheses";
    public static final String                                CLOSE_PAR       = "closeParentheses";
    public static final String                                STR_VALUE       = "strValue";
    public static final String                                MAX             = "max";
    public static final String                                MIN             = "min";
    public static final String                                AVERAGE         = "average";
    public static final String                                RANGE           = "range";
    public static final String                                CONCAT          = "concat";
    public static final String                                COMMA           = "comma";
    public static final String                                STRING_LITERAL  = "stringLiteral";
    public static final String                                PRINTF          = "printf";
    public static final String                                LEFT          = "left";
    public static final String                                RIGHT          = "right";
    public static final String                                TODAY          = "today";

    public static final String                                CELL_VALUE      = "cellValue";
    public static final String                                STRING          = "string";
    public static final String                                FORMULA         = "formula";
    public static final String                                MATH_EXPRESSION = "mathExpression";
    public static final String                                SUM             = "sum";
    public static final String                                TERM            = "term";
    public static final String                                FACTOR          = "factor";
    public static final String                                UNARY_SUM       = "unarySum";
    public static final String                                MAX_RANGE       = "maxRange";
    public static final String                                MIN_RANGE       = "minRange";
    public static final String                                AVERAGE_RANGE   = "averageRange";
    public static final String                                VALUES_LIST     = "valuesList";
    public static final String                                CONCAT_VALUES   = "concatValues";
    public static final String                                PRINTF_VALUES   = "printfValues";
    public static final String                                LEFT_VALUE   = "leftValue";
    public static final String                                RIGHT_VALUE   = "rightValue";
    public static final String                                TODAY_VALUE   = "todayValue";
    

    private static final List<String>                         terminals       = Collections
            .unmodifiableList(Arrays.asList(EQUAL, NUM_VALUE, REF_VALUE, PLUS, MINUS, TIMES, DIVIDE, EXPONENTIAL, OPEN_PAR,
                    CLOSE_PAR, STR_VALUE, MAX, MIN, AVERAGE, RANGE, CONCAT, COMMA, STRING_LITERAL, PRINTF, LEFT, RIGHT, TODAY));

    private static final List<String>                         regex           = Collections.unmodifiableList(Arrays
            .asList("^= *", " *[0-9]*\\.[0-9]+|[0-9]+ *", " *((\\[[a-zA-Z_0-9]+\\])?[a-zA-Z_0-9]+!)?[A-Z]+[0-9]+ *",
                    " *\\+ *", " *\\- *", " *\\* *", " *\\/ *", " *\\^ *", " *\\( *", " *\\) *", "^[^=].*$", " *MAX *", " *MIN *",
                    " *AVERAGE *", " *: *", " *CONCAT *", " *, *", " *\"[^=].*\" *", " *PRINTF *", " *LEFT *", " *RIGHT *", " *TODAY *"));

    private static final List<String>                         nonterminals    = Collections
            .unmodifiableList(Arrays.asList(CELL_VALUE, STRING, FORMULA, MATH_EXPRESSION, SUM, TERM, FACTOR, UNARY_SUM,
                    MAX_RANGE, MIN_RANGE, AVERAGE_RANGE, VALUES_LIST, CONCAT_VALUES, PRINTF_VALUES, LEFT_VALUE, RIGHT_VALUE, TODAY_VALUE));

    private static final List<CellParseTreeInterpreterAction> actions         = Collections.unmodifiableList(
            Arrays.asList(new ReturnChildValueAction(0), new ReturnStringAction(), new ReturnChildValueAction(1),
                    new ReturnChildValueAction(0), new ReturnBinOpOnChildrenAction(), new ReturnBinOpOnChildrenAction(),
                    new ReturnFactorAction(), new ReturnUnaryOpOnChildrenAction(), new ReturnMaxAction(),
                    new ReturnMinAction(), new ReturnAverageAction(), null, new ReturnConcatAction(), new ReturnFormattedStringAction(),
                    new ReturnLeftStringAction(), new ReturnRightStringAction(), new ReturnTodayAction()));

    // @formatter:off
 
    private static final String[][] productionRules = {    
        {"r1", CELL_VALUE, STRING},
        {"r2", CELL_VALUE, FORMULA},
        {"r3", STRING, STR_VALUE},
        {"r4", STRING, EQUAL},
        {"r5", FORMULA, EQUAL + " " + MATH_EXPRESSION},
        {"r6", MATH_EXPRESSION, SUM},
        {"r7", SUM, SUM + " " + PLUS + " " + TERM},
        {"r8", SUM, SUM + " " + MINUS + " " + TERM},
        {"r9", SUM, TERM},
        {"r10", TERM, TERM + " " + TIMES + " " + FACTOR},
        {"r11", TERM, TERM + " " + DIVIDE + " " + FACTOR},
        {"r12", TERM, TERM + " " + EXPONENTIAL + " " + FACTOR},
        {"r13", TERM, FACTOR},
        {"r14", FACTOR, NUM_VALUE},
        {"r15", FACTOR, REF_VALUE},
        {"r16", FACTOR, OPEN_PAR + " " + MATH_EXPRESSION + " " + CLOSE_PAR},
        {"r17", FACTOR, UNARY_SUM},
        {"r18", FACTOR, MAX_RANGE},
        {"r19", FACTOR, MIN_RANGE},
        {"r20", FACTOR, AVERAGE_RANGE},
        {"r21", FACTOR, CONCAT_VALUES},        
        {"r22", UNARY_SUM, PLUS + " " + FACTOR},
        {"r23", UNARY_SUM, MINUS + " " + FACTOR},
        {"r24", MAX_RANGE, MAX + " " + OPEN_PAR + " " + REF_VALUE +  " " + RANGE + " " + REF_VALUE + " " + CLOSE_PAR},
        {"r25", MIN_RANGE, MIN + " " + OPEN_PAR + " " + REF_VALUE + " " + RANGE + " " + REF_VALUE + " " + CLOSE_PAR},
        {"r26", AVERAGE_RANGE, AVERAGE + " " + OPEN_PAR + " " + REF_VALUE + " " + RANGE + " " + REF_VALUE + " " + CLOSE_PAR},
        {"r27", VALUES_LIST, MATH_EXPRESSION},
        {"r28", VALUES_LIST, STRING_LITERAL},
        {"r29", VALUES_LIST, VALUES_LIST + " " + COMMA + " " + MATH_EXPRESSION},
        {"r30", VALUES_LIST, VALUES_LIST + " " + COMMA + " " + STRING_LITERAL},
        {"r31", CONCAT_VALUES, CONCAT + " " + OPEN_PAR + " " + VALUES_LIST + " " + CLOSE_PAR},
        {"r32", FACTOR, PRINTF_VALUES},
        {"r33", PRINTF_VALUES, PRINTF + " " + OPEN_PAR + " " + STRING_LITERAL + " " + COMMA + " " + REF_VALUE + " " + CLOSE_PAR},
        {"r34", PRINTF_VALUES, PRINTF + " " + OPEN_PAR + " " + STRING_LITERAL + " " + COMMA + " " + REF_VALUE + " " 
                                                                                    + COMMA + " " + REF_VALUE + " " + CLOSE_PAR},
        {"r35", FACTOR, LEFT_VALUE},
        {"r36", LEFT_VALUE, LEFT + " " + OPEN_PAR + " " + REF_VALUE + " " + COMMA + " " + NUM_VALUE + " " + CLOSE_PAR},
        {"r37", LEFT_VALUE, LEFT + " " + OPEN_PAR + " " + STRING_LITERAL + " " + COMMA + " " + NUM_VALUE + " " + CLOSE_PAR},
        {"r38", FACTOR, RIGHT_VALUE},
        {"r39", RIGHT_VALUE, RIGHT + " " + OPEN_PAR + " " + REF_VALUE + " " + COMMA + " " + NUM_VALUE + " " + CLOSE_PAR},
        {"r40", RIGHT_VALUE, RIGHT + " " + OPEN_PAR + " " + STRING_LITERAL + " " + COMMA + " " + NUM_VALUE + " " + CLOSE_PAR},
        {"r41", FACTOR, TODAY_VALUE},
        {"r42", TODAY_VALUE, TODAY + " " + OPEN_PAR + " " + CLOSE_PAR},
    };
    
    // @formatter:on

    public static List<String> getTerminals() {
        return terminals;
    }

    public static List<String> getRegex() {
        return regex;
    }

    public static List<String> getNonterminals() {
        return nonterminals;
    }

    public static List<CellParseTreeInterpreterAction> getActions() {
        return actions;
    }

    @SuppressWarnings("CPD-END")
    public static List<List<String>> getProductionRules() {
        List<List<String>> result = new ArrayList<List<String>>();
        for (String[] rule : productionRules) {
            result.add(Collections.unmodifiableList(Arrays.asList(rule)));
        }

        return Collections.unmodifiableList(result);
    }

    public static final String startSymbol = CELL_VALUE;
}
