package ar.fiuba.tdd.tp1.expressioninterpreter;

import ar.fiuba.tdd.tp1.api.ISheetCell;
import ar.fiuba.tdd.tp1.api.InvalidBookNameException;
import ar.fiuba.tdd.tp1.api.InvalidCellIDException;
import ar.fiuba.tdd.tp1.api.InvalidFormulaException;
import ar.fiuba.tdd.tp1.api.InvalidSheetNameException;
import ar.fiuba.tdd.tp1.parser.ParseTree;
import ar.fiuba.tdd.tp1.workbook.CellDereferencer;

import java.util.List;
import java.util.Map;

public abstract class ReturnOpOnRangeAction implements CellParseTreeInterpreterAction {

    @Override
    public String apply(ParseTree tree, Map<String, CellParseTreeInterpreterAction> actionMap)
            throws InvalidSheetNameException, InvalidBookNameException, InvalidFormulaException, NumberFormatException {

        String startOfRangeCellID = tree.getChildren().get(2).getNode().getValue().trim();
        String endOfRangeCellID = tree.getChildren().get(4).getNode().getValue().trim();
        try {
            List<ISheetCell> cells = CellDereferencer.getInstance().dereferenceRange(startOfRangeCellID,
                    endOfRangeCellID);
            return opOnRange(cells);
        } catch (InvalidCellIDException e) {
            // Shouldn't get to this part.
            e.printStackTrace();
        }
        return null;
    }

    protected abstract String opOnRange(List<ISheetCell> cells)
            throws NumberFormatException, InvalidFormulaException, InvalidSheetNameException, InvalidBookNameException;

}
