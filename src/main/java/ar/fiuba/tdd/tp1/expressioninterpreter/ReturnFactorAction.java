package ar.fiuba.tdd.tp1.expressioninterpreter;

import ar.fiuba.tdd.tp1.api.ISheetCell;
import ar.fiuba.tdd.tp1.api.InvalidBookNameException;
import ar.fiuba.tdd.tp1.api.InvalidCellIDException;
import ar.fiuba.tdd.tp1.api.InvalidFormulaException;
import ar.fiuba.tdd.tp1.api.InvalidSheetNameException;
import ar.fiuba.tdd.tp1.parser.ParseTree;
import ar.fiuba.tdd.tp1.workbook.CellDereferencer;

import java.util.List;
import java.util.Map;

public class ReturnFactorAction implements CellParseTreeInterpreterAction {

    @Override
    public String apply(ParseTree tree, Map<String, CellParseTreeInterpreterAction> actionMap)
            throws InvalidSheetNameException, InvalidBookNameException, InvalidFormulaException, NumberFormatException {

        List<ParseTree> children = tree.getChildren();
        String childType = children.get(0).getNode().getType();

        if (childType == CellGrammar.NUM_VALUE) {
            return children.get(0).getNode().getValue().trim();
        } else if (childType == CellGrammar.REF_VALUE) {
            String referencedCellID = children.get(0).getNode().getValue().trim();
            try {
                ISheetCell referencedCell = CellDereferencer.getInstance().dereference(referencedCellID);
                return referencedCell.getResult();
            } catch (InvalidCellIDException e) {
                // Shouldn't get to this part.
                e.printStackTrace();
            }
            return null;
        } else if (childType == CellGrammar.OPEN_PAR) {
            return actionMap.get(CellGrammar.MATH_EXPRESSION).apply(children.get(1), actionMap);
        } else {
            return actionMap.get(childType).apply(children.get(0), actionMap);
        }
    }

}
