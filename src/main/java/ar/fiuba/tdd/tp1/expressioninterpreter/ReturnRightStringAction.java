package ar.fiuba.tdd.tp1.expressioninterpreter;

public class ReturnRightStringAction extends LeftOrRightStringAction {

    @Override
    public String modifyString(String originalString, int positions) {
        return originalString.substring(originalString.length() - positions, originalString.length());
    }

}
