package ar.fiuba.tdd.tp1.expressioninterpreter;

public class ReturnLeftStringAction extends LeftOrRightStringAction {

    @Override
    public String modifyString(String originalString, int positions) {
        return originalString.substring(0, positions);
    }

}
