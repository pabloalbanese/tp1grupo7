package ar.fiuba.tdd.tp1.expressioninterpreter;

import ar.fiuba.tdd.tp1.api.InvalidBookNameException;
import ar.fiuba.tdd.tp1.api.InvalidCellIDException;
import ar.fiuba.tdd.tp1.api.InvalidFormulaException;
import ar.fiuba.tdd.tp1.api.InvalidSheetNameException;
import ar.fiuba.tdd.tp1.lexer.Token;
import ar.fiuba.tdd.tp1.parser.ParseTree;
import ar.fiuba.tdd.tp1.workbook.CellDereferencer;

import java.util.Map;

public abstract class LeftOrRightStringAction implements CellParseTreeInterpreterAction {

    @Override
    public String apply(ParseTree tree, Map<String, CellParseTreeInterpreterAction> actionMap)
            throws InvalidSheetNameException, InvalidBookNameException, InvalidFormulaException, NumberFormatException {

        int positions = Integer.parseInt(tree.getChildren().get(4).getNode().getValue().trim());
        Token stringOrCellId = tree.getChildren().get(2).getNode();
        String originalString;
        if (stringOrCellId.getType() == CellGrammar.STRING_LITERAL) {
            originalString = stringOrCellId.getValue().replaceAll("\"", "");
        } else {
            try {
                String referencedCellID = stringOrCellId.getValue().trim();
                originalString = CellDereferencer.getInstance().dereference(referencedCellID).getResult();
            } catch (InvalidCellIDException e) {
                return null;
            }
        }
        return modifyString(originalString, positions);
    }

    public abstract String modifyString(String originalString, int positions);

}
