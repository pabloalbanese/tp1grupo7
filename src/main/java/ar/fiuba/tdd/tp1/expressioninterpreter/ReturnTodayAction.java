package ar.fiuba.tdd.tp1.expressioninterpreter;

import ar.fiuba.tdd.tp1.api.InvalidBookNameException;
import ar.fiuba.tdd.tp1.api.InvalidFormulaException;
import ar.fiuba.tdd.tp1.api.InvalidSheetNameException;
import ar.fiuba.tdd.tp1.parser.ParseTree;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Map;
import java.util.TimeZone;

public class ReturnTodayAction implements CellParseTreeInterpreterAction {
    @Override
    public String apply(ParseTree tree, Map<String, CellParseTreeInterpreterAction> actionMap) 
            throws InvalidSheetNameException, InvalidBookNameException, InvalidFormulaException, NumberFormatException {
     
        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mmZ");
        df.setTimeZone(tz);
        return df.format(new Date());
    }
}
