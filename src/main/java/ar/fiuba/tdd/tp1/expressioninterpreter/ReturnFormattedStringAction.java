package ar.fiuba.tdd.tp1.expressioninterpreter;

import ar.fiuba.tdd.tp1.api.ISheetCell;
import ar.fiuba.tdd.tp1.api.InvalidBookNameException;
import ar.fiuba.tdd.tp1.api.InvalidCellIDException;
import ar.fiuba.tdd.tp1.api.InvalidFormulaException;
import ar.fiuba.tdd.tp1.api.InvalidSheetNameException;

import ar.fiuba.tdd.tp1.parser.ParseTree;

import ar.fiuba.tdd.tp1.workbook.CellDereferencer;

import java.util.Map;

public class ReturnFormattedStringAction implements CellParseTreeInterpreterAction {

    @Override
    public String apply(ParseTree tree, Map<String, CellParseTreeInterpreterAction> actionMap)
            throws InvalidSheetNameException, InvalidBookNameException, InvalidFormulaException, NumberFormatException {
        
        int childsNumber = tree.getChildren().size();
        int paramsNumber = childsNumber - 5 - (childsNumber - 5) / 2;
        String templateString = tree.getChildren().get(2).getNode().getValue().replaceAll("\"", "");
        for (int i = 0; i < paramsNumber; i++) {
            int posParam = 4 + 2 * i;
            String referencedCellID = tree.getChildren().get(posParam).getNode().getValue().trim();
            try {
                ISheetCell referencedCell = CellDereferencer.getInstance().dereference(referencedCellID);
                String valueToPut = referencedCell.getResult();
                templateString = templateString.replaceAll("\\$" + i, valueToPut);
            } catch (InvalidCellIDException e) {
                return null;
            }
        }
        return templateString;
    }

}
