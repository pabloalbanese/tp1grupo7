package ar.fiuba.tdd.tp1.workbook;

import ar.fiuba.tdd.tp1.api.DuplicateBookNameException;
import ar.fiuba.tdd.tp1.api.DuplicateSheetNameException;
import ar.fiuba.tdd.tp1.api.ISpreadsheet;
import ar.fiuba.tdd.tp1.api.IWorkbook;
import ar.fiuba.tdd.tp1.api.IWorkbookController;
import ar.fiuba.tdd.tp1.api.InvalidBookNameException;
import ar.fiuba.tdd.tp1.api.InvalidCellIDException;
import ar.fiuba.tdd.tp1.api.InvalidFormulaException;
import ar.fiuba.tdd.tp1.api.InvalidSheetNameException;
import ar.fiuba.tdd.tp1.importexport.CsvCell;
import ar.fiuba.tdd.tp1.importexport.CsvSheet;
import ar.fiuba.tdd.tp1.importexport.ICSVable;
import ar.fiuba.tdd.tp1.importexport.IJsonable;
import ar.fiuba.tdd.tp1.importexport.JsonBook;
import ar.fiuba.tdd.tp1.undoredo.UndoRedo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.*;

public class ConcreteWorkbookController implements IWorkbookController {

    private IWorkbookBuilder builder;
    private List<IWorkbook>  books;
    private UndoRedo         changesTracker;
    private Map<String, Range> ranges;

    public ConcreteWorkbookController(IWorkbookBuilder workbookBuilder, UndoRedo changesTracker) {
        this.builder = workbookBuilder;
        this.books = new ArrayList<>();
        this.changesTracker = changesTracker;
        ranges = new HashMap<String, Range>();
    }

    @Override
    public void addBook(String name) throws DuplicateBookNameException {
        IWorkbook bookToRemove = findBook(name);

        if (bookToRemove != null) {
            throw new DuplicateBookNameException();
        }

        IWorkbook newBook = this.builder.buildWorkbook(name);
        this.books.add(newBook);
    }

    @Override
    public void removeBook(String name) throws InvalidBookNameException {
        IWorkbook bookToRemove = findBook(name);

        if (bookToRemove == null) {
            throw new InvalidBookNameException();
        }

        this.books.remove(bookToRemove);
    }

    @Override
    public IWorkbook getBook(String name) throws InvalidBookNameException {
        IWorkbook bookToGet = findBook(name);

        if (bookToGet == null) {
            throw new InvalidBookNameException();
        }

        return bookToGet;
    }

    @Override
    public List<IWorkbook> getBooks() {
        return Collections.unmodifiableList(this.books);
    }

    @Override
    public void undo() throws Exception {
        changesTracker.undo();
    }

    @Override
    public void redo() throws Exception {
        changesTracker.redo();
    }

    private IWorkbook findBook(String name) {
        for (IWorkbook book : this.books) {
            if (book.getName().equals(name)) {
                return book;
            }
        }
        return null;
    }

    @Override
    public void exportBookToJson(String name, String path) throws InvalidBookNameException, IOException {
        IJsonable jsonbook = new JsonBook(this.getBook(name));
        
        File file = new File(path);
        Writer writer = new OutputStreamWriter(new FileOutputStream(file), "UTF-8");
        
        writer.write(jsonbook.toJson());
        writer.flush();
        writer.close();
    }

    @Override
    public void importBookFromJson(String path) throws IOException {
        BufferedReader bfreader = this.getFileBufferedReader(path);
        
        String readLine = "";
        StringBuffer data = new StringBuffer();
        while ( (readLine = bfreader.readLine()) != null ) {
            data.append(readLine);
        }
        bfreader.close();
        
        JsonBook jsonbook = new JsonBook();
        jsonbook.fromJson(data.toString());
        
        try {
            this.removeBook(jsonbook.getName());
        } catch (InvalidBookNameException e) {
            e.printStackTrace();
        }
        
        this.books.add(jsonbook.getBook());
    }

    @Override
    public void saveSheetAsCSV(String workBookName, String sheetName, String path) throws IOException {
        ICSVable csvsheet = new CsvSheet();
        try {
            csvsheet = new CsvSheet(this.getBook(workBookName).getSheet(sheetName, true));
        } catch (InvalidSheetNameException | InvalidBookNameException e) {
            //e.printStackTrace();
        } 
        
        File file = new File(path);
        Writer writer = new OutputStreamWriter(new FileOutputStream(file), "UTF-8");
        try {
            writer.close();
            writer = new OutputStreamWriter(new FileOutputStream(file), "UTF-8");
        } catch (UnsupportedEncodingException | FileNotFoundException e) {
            //e.printStackTrace();
        }
        
        writer.write(csvsheet.toCSV());
        writer.flush();
        writer.close();        
    }

    @Override
    public void loadSheetFromCSV(String workBookName, String sheetName, String path) throws IOException {
        BufferedReader bfreader = this.getFileBufferedReader(path);
        
        String readLine = "";
        StringBuffer data = new StringBuffer();
        while ( (readLine = bfreader.readLine()) != null ) {
            data.append(readLine);
            data.append("\n");
        }
        bfreader.close();
        
        CsvSheet csvsheet = new CsvSheet();
        csvsheet.fromCSV(data.toString());
        
        ISpreadsheet sheet = this.getValidSheet(workBookName, sheetName);
        
        try {        
            for (CsvCell csvcell : csvsheet.getCells()) {
                sheet.getCell(csvcell.getId(), true).setValue(csvcell.getValue());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void createRange(String name, String value) {
        String[] rangeValues = value.split(":");
        String firstOfRangeCellID = rangeValues[0];
        String endOfRangeCellID = rangeValues[1];
        Range range = new Range(firstOfRangeCellID, endOfRangeCellID);
        ranges.put(name, range);
    }

    @Override
    public Range getRange(String name) {
        if (ranges.containsKey(name)) {
            return ranges.get(name);
        } else {
            return null;
        }
    }

    public ISpreadsheet getValidSheet(String workBookName, String sheetName) {
        ISpreadsheet sheet = null;
        try {
            this.getBook(workBookName).addSheet(sheetName);
        } catch (DuplicateSheetNameException | InvalidBookNameException e1) {
            //e1.printStackTrace();
        }
        
        try {
            sheet = this.getBook(workBookName).getSheet(sheetName, true);
        } catch (InvalidSheetNameException | InvalidBookNameException e1) {
            //e1.printStackTrace();
        }      
        return sheet;
    }
    
    public BufferedReader getFileBufferedReader(String path) throws UnsupportedEncodingException, FileNotFoundException {
        File file = new File(path);
        Reader reader = new InputStreamReader(new FileInputStream(file), "UTF-8");
        //reader.close();
        return new BufferedReader(reader);
    }
    
}
