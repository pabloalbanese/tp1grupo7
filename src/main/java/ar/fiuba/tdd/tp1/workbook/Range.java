package ar.fiuba.tdd.tp1.workbook;

public class Range {
    private String firstOfRangeCellID;
    private String endOfRangeCellID;

    public Range(String first, String end) {
        this.firstOfRangeCellID = first;
        this.endOfRangeCellID = end;
    }

    public String getFirstOfRangeCellID() {
        return firstOfRangeCellID;
    }

    public String getEndOfRangeCellID() {
        return endOfRangeCellID;
    }
}
