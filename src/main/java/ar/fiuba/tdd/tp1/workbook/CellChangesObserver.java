package ar.fiuba.tdd.tp1.workbook;

import ar.fiuba.tdd.tp1.observerpattern.IObserver;
import ar.fiuba.tdd.tp1.undoredo.CellChange;
import ar.fiuba.tdd.tp1.undoredo.UndoRedo;

// Observes a cell, watching for it to change values.
// When this happens, it pushes such change into it's changesTracker,
// so as to be able to undo the change.
public class CellChangesObserver implements IObserver {

    UndoRedo     changesTracker;
    ConcreteCell observedCell;

    public CellChangesObserver(UndoRedo changesTracker, ConcreteCell observedCell) {
        this.changesTracker = changesTracker;
        this.observedCell = observedCell;
    }

    @Override
    public void update(ObservableEvent event) {
        if (event != IObserver.ObservableEvent.CELL_CHANGE) {
            return;
        }
        String oldValue = this.observedCell.getPreviousRawValue();
        String newValue = this.observedCell.getRawValue();
        CellChange change = new CellChange(this.observedCell, oldValue, newValue);
        this.changesTracker.pushChange(change);
    }

}
