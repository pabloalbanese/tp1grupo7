package ar.fiuba.tdd.tp1.workbook;

import ar.fiuba.tdd.tp1.api.ISheetCell;
import ar.fiuba.tdd.tp1.api.ISpreadsheet;

// Interface for building sheets and cells.
// Workbooks and Spreadsheets should have one assigned, so
// as to easily be able to change the implementation of sheets
// and cells without having to change their containers (books
// and sheets respectively).
public interface IWorkbookElementBuilder {

    public ISpreadsheet buildSheet(String name);

    public ISheetCell buildCell(String id);
}
