package ar.fiuba.tdd.tp1.workbook;

import ar.fiuba.tdd.tp1.api.ISpreadsheet;
import ar.fiuba.tdd.tp1.observerpattern.IObserver;
import ar.fiuba.tdd.tp1.undoredo.SheetChange;
import ar.fiuba.tdd.tp1.undoredo.UndoRedo;

//Observes a book, watching for it to get focus (ie: become the active book).
//When this happens, it changes the active book in CellDereferencer.
public class BookFocusObserver implements IObserver {

    ConcreteWorkbook observedBook;
    private UndoRedo changesTracker;

    public BookFocusObserver(ConcreteWorkbook book, UndoRedo changesTracker) {
        this.observedBook = book;
        this.changesTracker = changesTracker;
    }

    @Override
    public void update(ObservableEvent event) {
        if (event == IObserver.ObservableEvent.BOOK_FOCUS_CHANGE) {
            CellDereferencer.getInstance().setActiveBook(this.observedBook.getName());
        } else if (event == IObserver.ObservableEvent.SHEET_ADDED) {

            ISpreadsheet addedSheet = this.observedBook.getChangedSheet();
            SheetChange change = new SheetChange(this.observedBook, addedSheet, true);
            this.changesTracker.pushChange(change);
        } else if (event == IObserver.ObservableEvent.SHEET_REMOVED) {

            ISpreadsheet removedSheet = this.observedBook.getChangedSheet();
            SheetChange change = new SheetChange(this.observedBook, removedSheet, false);
            this.changesTracker.pushChange(change);
        }
    }

}
