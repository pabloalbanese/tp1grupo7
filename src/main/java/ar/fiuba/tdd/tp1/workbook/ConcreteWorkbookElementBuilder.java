package ar.fiuba.tdd.tp1.workbook;

import ar.fiuba.tdd.tp1.api.ISheetCell;
import ar.fiuba.tdd.tp1.api.ISpreadsheet;
import ar.fiuba.tdd.tp1.undoredo.UndoRedo;

public class ConcreteWorkbookElementBuilder implements IWorkbookElementBuilder {

    UndoRedo changesTracker;

    public ConcreteWorkbookElementBuilder(UndoRedo changesTracker) {
        this.changesTracker = changesTracker;
    }

    @Override
    public ISpreadsheet buildSheet(String name) {

        ConcreteSpreadsheet newSheet = new ConcreteSpreadsheet(name, this);
        SheetFocusObserver sheetObserver = new SheetFocusObserver(newSheet);
        newSheet.attach(sheetObserver);
        return newSheet;
    }

    @Override
    public ISheetCell buildCell(String id) {
        ConcreteCell newCell = new ConcreteCell(id);
        CellChangesObserver cellObserver = new CellChangesObserver(this.changesTracker, newCell);
        newCell.attach(cellObserver);
        return newCell;
    }
}
