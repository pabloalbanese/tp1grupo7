package ar.fiuba.tdd.tp1.workbook;

import ar.fiuba.tdd.tp1.api.*;
import ar.fiuba.tdd.tp1.expressioninterpreter.ExpressionInterpreter;
import ar.fiuba.tdd.tp1.observerpattern.IObservable;
import ar.fiuba.tdd.tp1.observerpattern.IObserver;
import ar.fiuba.tdd.tp1.observerpattern.IObserver.ObservableEvent;
import ar.fiuba.tdd.tp1.parser.ParseTree;

import java.util.ArrayList;
import java.util.List;

public class ConcreteCell implements IObservable, ISheetCell {

    private String          id;
    private String          previousValue;
    private String          value;
    private ParseTree       parsedValue;
    private List<IObserver> observers;
    private IFormatter      formatter;
    private String          type;

    public ConcreteCell(String id) {
        this.id = id;
        this.previousValue = "";
        this.value = "";
        this.parsedValue = null;
        this.observers = new ArrayList<>();
        this.formatter = new NullFormatter();
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public void setValue(String value) throws Exception {
        this.previousValue = this.value;

        IWorkbookController controller = CellDereferencer.getInstance().getController();
        String rangeName = this.getRangeName(value);
        Range range = controller.getRange(rangeName);
        if (range != null) {
            String formulaName = this.getFormulaName(value);
            this.value = formulaName + "(" + range.getFirstOfRangeCellID() + ":" + range.getEndOfRangeCellID() + ")";
        } else {
            this.value = value;
        }

        this.parsedValue = ExpressionInterpreter.getInstance().parse(this.value);
        notifyObservers(IObserver.ObservableEvent.CELL_CHANGE);
    }

    private String getFormulaName(String value) {
        int end = value.indexOf("(");
        if (end > 0) {
            return value.substring(0, end);
        } else {
            return "";
        }
    }

    private String getRangeName(String value) {
        int begin = value.indexOf("(");
        int end = value.indexOf(")");
        if ((begin > 0) && (end > 0)) {
            return value.substring(begin + 1, end);
        } else {
            return "";
        }
    }

    @Override
    public String getResult() throws InvalidFormulaException, InvalidSheetNameException, InvalidBookNameException {
        return ExpressionInterpreter.getInstance().interpret(this.parsedValue);
    }

    @Override
    public String getRawValue() {
        return this.value;
    }

    public ParseTree getParsedValue() {
        return parsedValue;
    }

    String getPreviousRawValue() {
        return this.previousValue;
    }

    @Override
    public void setFormatter(IFormatter formatter) {
        this.formatter = formatter;
    }
    
    @Override
    public void attach(IObserver observer) {
        this.observers.add(observer);
    }
    
    @Override
    public String getFormattedResult() throws InvalidFormulaException, InvalidSheetNameException, InvalidBookNameException {
        String value = this.getResult();
        return this.formatter.applyFormat(value);
    }
    
    @Override
    public void detach(IObserver observer) {
        this.observers.remove(observer);
    }
    
    @Override
    public IFormatter getFormatter() {
        return this.formatter;
    }
    
    @Override
    public void notifyObservers(ObservableEvent event) {
        for (IObserver obs : this.observers) {
            obs.update(event);
        }
    }

    @Override
    public void setType(String type) {
        this.type = type;
        
        switch (type) {
            case "Date": setFormatter(new DateFormatter()); break;
            case "Currency": setFormatter(new MoneyFormatter()); break;
            case "String": setFormatter(new StringFormatter()); break;
            case "Number": setFormatter(new NumberFormatter()); break;
            default: setFormatter(new NullFormatter());
        }
    }

    @Override
    public String getType() {
        return this.type;
    }
}
