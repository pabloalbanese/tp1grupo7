package ar.fiuba.tdd.tp1.workbook;

import ar.fiuba.tdd.tp1.api.IFormatter;

import java.text.NumberFormat;

public class NumberFormatter implements IFormatter {

    private int decimals    = 0;
    private String type     = "Number";
    
    @Override
    public String applyFormat(String value) {
        return null;
    }

    public void setDecimals(int decimals) {
        this.decimals = decimals;
    }

    public int getDecimals() {
        return this.decimals;
    }
    
    @Override
    public String getType() {
        return this.type;
    }
    
    public String toString() {
        return "\"Number.Decimal\": " + String.valueOf(decimals);
    }

    @Override
    public void setNewFormat(String formatter, String format) {
        switch (formatter) {
            case "decimals": this.decimals = Integer.parseInt(format); break;
            default: System.out.println("format '" + format + "undefined");
        }
    }
}
