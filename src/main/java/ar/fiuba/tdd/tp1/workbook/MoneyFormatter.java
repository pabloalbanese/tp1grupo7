package ar.fiuba.tdd.tp1.workbook;

import ar.fiuba.tdd.tp1.api.IFormatter;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class MoneyFormatter implements IFormatter {

    private String coin = "$";
    private String type = "Money";
    private Integer    decimals = 0;
    //private String separator = ".";
    
    public void setCoin(String coin) {
        this.coin = coin;
    }
    
    public String getCoin() {
        return this.coin;
    }
    
    @Override
    public String applyFormat(String value) {
        try {
            Double.valueOf(value).toString();
        } catch (NumberFormatException ex) {
            return "Error:BAD_CURRENCY";
        }
        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols();
        otherSymbols.setDecimalSeparator('.');
        otherSymbols.setGroupingSeparator('.');
        
        DecimalFormat formatter = new DecimalFormat();
        formatter.setMinimumFractionDigits(this.decimals);
        formatter.setDecimalFormatSymbols(otherSymbols);
        String date = formatter.format(Double.valueOf(value)).toString();
        return coin + " " + date;
    }
    
    public String toString() {
        return "\"Money.Coin\": \"" + coin + "\"";
    }
    
    @Override
    public String getType() {
        return this.type;
    }
    
    @Override
    public void setNewFormat(String formatter, String format) {
        switch (formatter) {
            case "symbol": this.coin = format; break;
            case "decimal": this.decimals = Integer.valueOf(format); break;
            //case "separator": this.separator = format; break;
            default: ;
        }
    }
}
