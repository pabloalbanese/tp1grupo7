package ar.fiuba.tdd.tp1.undoredo;

import ar.fiuba.tdd.tp1.api.ISpreadsheet;
import ar.fiuba.tdd.tp1.workbook.ConcreteWorkbook;

public class SheetChange implements IUndoable {

    private ConcreteWorkbook book;
    private ISpreadsheet     changedSheet;
    private boolean          added;

    public SheetChange(ConcreteWorkbook book, ISpreadsheet addedSheet, boolean added) {
        this.book = book;
        this.changedSheet = addedSheet;
        this.added = added;
    }

    @Override
    public void revert() {
        if (!added) {
            book.addSheet(this.changedSheet);
        } else {
            book.removeSheet(this.changedSheet);
        }
    }

    @Override
    public void apply() {
        if (!added) {
            book.removeSheet(this.changedSheet);
        } else {
            book.addSheet(this.changedSheet);
        }
    }

}
