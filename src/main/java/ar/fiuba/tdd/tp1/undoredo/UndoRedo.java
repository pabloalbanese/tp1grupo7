package ar.fiuba.tdd.tp1.undoredo;

import java.util.Stack;

// UndoRedo acts as Command pattern's Invoker.
// Workbook should acts as Command pattern's Client.
public class UndoRedo {

    private Stack<IUndoable> undoChanges = new Stack<IUndoable>();
    private Stack<IUndoable> redoChanges = new Stack<IUndoable>();
    private boolean          working     = false;

    public void pushChange(IUndoable change) {
        if (working) {
            return;
        }
        this.redoChanges.clear();
        this.undoChanges.push(change);
    }

    public void undo() throws Exception {
        try {
            working = true;
            
            IUndoable change = undoChanges.pop();
            System.out.println(change);
            redoChanges.push(change);
            change.revert();
            working = false;
        } catch (Exception e) {
            working = false;
            throw new Exception("There are no more expressions to undo");
        }
    }

    public void redo() throws Exception {
        try {
            working = true;
            IUndoable change = redoChanges.pop();
            System.out.println(change.toString());
            undoChanges.push(change);
            change.apply();
            working = false;
        } catch (Exception e) {
            working = false;
            throw new Exception("There are no more expressions to redo");
        }
    }
}
