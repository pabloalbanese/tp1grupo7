package ar.fiuba.tdd.tp1.api;

// Interface that represents a workbookController builder.
// Any class that wants to build a workbookController should 
// implement this interface.
public interface IWorkbookControllerBuilder {

    public IWorkbookController buildWorkbookController();
}
