package ar.fiuba.tdd.tp1.api;

// Exception thrown when the lexical analyzer doesn't recognize a sequence
// of characters as a valid grammar token.
public class UnrecognizedTokenException extends Exception {

    private static final long serialVersionUID = -3347140720413663755L;

}
