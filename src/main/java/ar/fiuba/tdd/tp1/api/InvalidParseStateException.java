package ar.fiuba.tdd.tp1.api;

// Exception thrown when the parser finds a sequence of symbols
// that it doesn't know how to parse.
public class InvalidParseStateException extends Exception {

    private static final long serialVersionUID = 1L;

}
