package ar.fiuba.tdd.tp1.api;

import java.util.HashMap;

public interface ICycleDetector {

    public void addCells(HashMap<String, ISheetCell> cells);
    
    public boolean hasCycle(String cellId);
}
