package ar.fiuba.tdd.tp1.api;

// Thrown when trying to get a cell with invalid ID.
public class InvalidCellIDException extends Exception {

    private static final long serialVersionUID = 8630250603522721877L;

}
