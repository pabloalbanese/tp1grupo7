package ar.fiuba.tdd.tp1.api;

import ar.fiuba.tdd.tp1.workbook.Range;

import java.io.IOException;
import java.util.List;

// Defines what a workbookController should be able to do.
public interface IWorkbookController {

    public void addBook(String name) throws DuplicateBookNameException;

    public void removeBook(String name) throws InvalidBookNameException;

    public IWorkbook getBook(String name) throws InvalidBookNameException;

    public List<IWorkbook> getBooks();

    public void undo() throws Exception;

    public void redo() throws Exception;
    
    public void exportBookToJson(String name, String path) throws InvalidBookNameException, IOException;
    
    public void importBookFromJson(String path) throws IOException;
    
    public void saveSheetAsCSV(String workBookName, String sheetName, String path)  throws IOException;
    
    public void loadSheetFromCSV(String workBookName, String sheetName, String path) throws IOException;

    public void createRange(String name, String value);

    public Range getRange(String name);
}
