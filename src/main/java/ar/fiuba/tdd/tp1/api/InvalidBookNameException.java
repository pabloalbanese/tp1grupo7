package ar.fiuba.tdd.tp1.api;

// Thrown when trying to get or remove a book with unrecognized name.
public class InvalidBookNameException extends Exception {

    private static final long serialVersionUID = 1L;

}
