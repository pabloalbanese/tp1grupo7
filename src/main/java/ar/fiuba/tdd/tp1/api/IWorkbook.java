package ar.fiuba.tdd.tp1.api;

import java.util.List;

// Defines a minimum functionality for what a workbook should be able to do.
public interface IWorkbook {

    public String getName();

    public void addSheet(String name) throws DuplicateSheetNameException;

    public void removeSheet(String name) throws InvalidSheetNameException;

    public ISpreadsheet getSheet(String name, boolean pullFocus) throws InvalidSheetNameException;

    public List<ISpreadsheet> getSheets();
    
    public String getVersion();
    
    public void setVersion(String version);
}