package ar.fiuba.tdd.tp1.api;

// Thrown when trying to get or remove a sheet with an unrecognized name.
public class InvalidSheetNameException extends Exception {

    private static final long serialVersionUID = 4251237931483140096L;

}
