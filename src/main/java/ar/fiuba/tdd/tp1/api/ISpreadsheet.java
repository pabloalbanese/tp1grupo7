package ar.fiuba.tdd.tp1.api;

import java.util.HashMap;

// Defines a minimum functionality for what a spreadsheet should be able to do.
public interface ISpreadsheet {

    public String getName();

    public ISheetCell getCell(String id, boolean pullFocus) throws InvalidCellIDException;

    public ISheetCell getCell(String column, int row) throws InvalidCellIDException;
    
    public HashMap<String, ISheetCell> getCells();

}