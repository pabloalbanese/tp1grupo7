package ar.fiuba.tdd.tp1.importexport;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import ar.fiuba.tdd.tp1.api.IFormatter;
import ar.fiuba.tdd.tp1.workbook.DateFormatter;
import ar.fiuba.tdd.tp1.workbook.MoneyFormatter;

public class JsonMoneyFormatter extends JsonFormatter {
    
    @Expose @SerializedName("Money.Coin") private String coin = null;
    
    public JsonMoneyFormatter() {}
    
    public JsonMoneyFormatter(IFormatter formatter) {
        MoneyFormatter moneyFormatter = (MoneyFormatter) formatter;
        this.coin = moneyFormatter.getCoin();
    }
    
    @Override
    public void copyFrom(Object desjonizedObject) {
        JsonMoneyFormatter jsonMoneyFormatter = (JsonMoneyFormatter) desjonizedObject;
        this.coin = jsonMoneyFormatter.getCoin();
    }
    
    public String getCoin() {
        return this.coin;
    }

    @Override
    public IFormatter getIFormatter() {
        MoneyFormatter moneyFormatter = new MoneyFormatter();
        moneyFormatter.setCoin(this.coin);
        return moneyFormatter;
    }
}
