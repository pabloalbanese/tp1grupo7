package ar.fiuba.tdd.tp1.importexport;

public interface IJsonable {

    public String toJson();
    
    public void fromJson(String jsonstring);
}
