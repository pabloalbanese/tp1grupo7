package ar.fiuba.tdd.tp1.importexport;

import com.google.gson.annotations.Expose;

import ar.fiuba.tdd.tp1.api.ISheetCell;
import ar.fiuba.tdd.tp1.api.ISpreadsheet;
import ar.fiuba.tdd.tp1.workbook.ConcreteSpreadsheet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

public class CsvSheet implements ICSVable {

    @Expose List<CsvCell> csvcells = null;
    
    public CsvSheet() {
        this.csvcells = new ArrayList<CsvCell>();
    }
    
    public CsvSheet(ISpreadsheet sheet) {
        ConcreteSpreadsheet concreteSheet = (ConcreteSpreadsheet) sheet;
        this.csvcells = new ArrayList<CsvCell>();
        for (ISheetCell cell : concreteSheet.getListCells()) {
            CsvCell csvcell = new CsvCell(cell);
            csvcell.setSheetName(concreteSheet.getName());
            this.csvcells.add( csvcell );
        }
    }
    
    @Override
    public String toCSV() {
        StringBuilder builder = new StringBuilder();
        for (CsvCell csvcell : this.csvcells) {
            builder.append(csvcell.toCSV());
            builder.append("\n");
        }
        return builder.toString();
    }

    @Override
    public void fromCSV(String csv) {
        StringReader csvreader = new StringReader(csv);
        BufferedReader bfreader = new BufferedReader(csvreader);
        
        String stringcell = "";
        try {
            while ( (stringcell = bfreader.readLine()) != null) {
                System.out.println("stringcell:" + stringcell);
                CsvCell csvcell = new CsvCell();
                csvcell.fromCSV(stringcell);
                
                this.csvcells.add(csvcell);
            }
            
            bfreader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
//    public HashMap<String, ISheetCell> getCells() {
//        HashMap<String, ISheetCell> cells = new HashMap<>();
//        for (CsvCell csvcell : this.csvcells) {
//            cells.put(csvcell.getId(), csvcell.getCell());
//        }
//        return cells;
//    }
    
    public List<CsvCell> getCells() {
        return this.csvcells;
    }
}
