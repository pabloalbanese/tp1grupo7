package ar.fiuba.tdd.tp1.importexport;

public abstract class Jsonable implements IJsonable {
    
    @Override
    public String toJson() {
        return Jsonizer.jsonize(this);
    }

    @Override
    public void fromJson(String jsonstring) {
        Object desjsonized = Jsonizer.desjsonize(jsonstring, this);
        this.copyFrom(desjsonized);
    }
    
    public abstract void copyFrom(Object desjonizedObject);
}
