package ar.fiuba.tdd.tp1.importexport;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import ar.fiuba.tdd.tp1.api.IFormatter;
import ar.fiuba.tdd.tp1.workbook.DateFormatter;

public class JsonDateFormatter extends JsonFormatter {

    @Expose @SerializedName("Date.Format") private String dateFormat = null;
    
    public JsonDateFormatter() {}
    
    public JsonDateFormatter(IFormatter formatter) {
        DateFormatter dateFormatter = (DateFormatter) formatter;
        this.dateFormat = dateFormatter.getDateFormat();
    }
    
    public String getDateFormat() {
        return this.dateFormat;
    }
    
    @Override
    public void copyFrom(Object desjonizedObject) {
        JsonDateFormatter jsonDateFormatter = (JsonDateFormatter) desjonizedObject;
        this.dateFormat = jsonDateFormatter.getDateFormat();
    }
    
    public String toString() {
        return "dateFormatter";
    }

    @Override
    public IFormatter getIFormatter() {
        DateFormatter dateFormatter = new DateFormatter();
        dateFormatter.setDateFormat(this.dateFormat);
        return dateFormatter;
    }
}
