package ar.fiuba.tdd.tp1.importexport;

import ar.fiuba.tdd.tp1.api.ISheetCell;
import ar.fiuba.tdd.tp1.api.InvalidBookNameException;
import ar.fiuba.tdd.tp1.api.InvalidFormulaException;
import ar.fiuba.tdd.tp1.api.InvalidSheetNameException;
import ar.fiuba.tdd.tp1.workbook.ConcreteCell;
import ar.fiuba.tdd.tp1.workbook.StringFormatter;

public class CsvCell implements ICSVable {

    private String sheetName = "";
    private String id = "";
    private String value = "";
    
    public CsvCell() {}
    
    public CsvCell(ISheetCell cell) {
        this.id = cell.getId();
        try {
            this.value = cell.getFormattedResult();
        } catch (InvalidFormulaException | InvalidSheetNameException | InvalidBookNameException e) {
            try {
                this.value = cell.getResult();
            } catch (InvalidFormulaException | InvalidSheetNameException | InvalidBookNameException e1) {
                this.value = cell.getRawValue();
            }
        }
    }
    
    public String getId() {
        return this.id;
    }
    
    public String getValue() {
        return this.value;
    }
    
    public void setSheetName(String sheetName) {
        this.sheetName = sheetName;
    }
    
    @Override
    public String toCSV() {
        return sheetName + "," + id + "," + value;
    }

    @Override
    public void fromCSV(String csv) {
        String[] values = csv.split(",");
        this.sheetName = values[0];
        this.id = values[1];
        this.value = values[2];
    }
    
    public ISheetCell getCell() {
        ISheetCell cell = new ConcreteCell(this.id);
        try {
            cell.setValue(this.value);
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        cell.setFormatter(new StringFormatter());
        return cell;
    }

}
