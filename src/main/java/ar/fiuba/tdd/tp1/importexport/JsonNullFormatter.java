package ar.fiuba.tdd.tp1.importexport;

import ar.fiuba.tdd.tp1.api.IFormatter;
import ar.fiuba.tdd.tp1.workbook.NullFormatter;

public class JsonNullFormatter extends JsonFormatter {

    public JsonNullFormatter() {}
    
    @Override
    public void copyFrom(Object desjonizedObject) {}

    @Override
    public IFormatter getIFormatter() {
        return new NullFormatter();
    }

}
