package ar.fiuba.tdd.tp1.lexer;

import ar.fiuba.tdd.tp1.api.UnrecognizedTokenException;
import ar.fiuba.tdd.tp1.contextfreegrammar.ExtendedContextFreeGrammar;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class ConcreteLexer implements Lexer {

    List<TokenIdentifier> identifiers = null;
    Queue<Token>          tokens      = null;
    int                   currentPosition;

    public ConcreteLexer(ExtendedContextFreeGrammar extGrammar) {
        identifiers = new LinkedList<TokenIdentifier>();
        this.tokens = new LinkedList<Token>();
        registerLexemes(extGrammar);
        this.currentPosition = 0;
    }

    public void registerLexemes(ExtendedContextFreeGrammar extGrammar) {

        for (String lexeme : extGrammar.getTerminalSymbols()) {
            String regex = extGrammar.getRegexForTerminal(lexeme);
            this.identifiers.add(new TokenIdentifier(lexeme, regex));
        }
    }

    @Override
    public Queue<Token> lex(String input) throws Exception {

        this.clear();

        while (this.hasNextToken(input)) {
            this.tokens.add(getNextToken(input));
        }

        Token eofToken = new Token("$", "");
        this.tokens.add(eofToken);
        return this.tokens;
    }

    private void clear() {

        this.currentPosition = 0;
        this.tokens.clear();
    }

    private boolean hasNextToken(String input) {

        return this.currentPosition < input.length();
    }

    private Token getNextToken(String input) throws Exception {

        TokenIdentifier bestMatch = getBestMatch(input);

        try {
            return bestMatch.getToken();
        } catch (Exception e) {
            System.out.println(input.substring(currentPosition));
            throw new UnrecognizedTokenException();
        }
    }

    private TokenIdentifier getBestMatch(String input) {

        int maxTokenLength = 0;
        TokenIdentifier bestMatch = null;

        for (TokenIdentifier identifier : this.identifiers) {
            try {
                if ((identifier.lookForToken(input, this.currentPosition) == true)
                        && (identifier.tokenLength() > maxTokenLength)) {
                    maxTokenLength = identifier.tokenLength();
                    bestMatch = identifier;
                }
            } catch (Exception e) {
                // Nothing to do, this exception shouldn't be raised by this
                // piece of code.
            }
        }

        this.currentPosition += maxTokenLength;
        return bestMatch;
    }
}
