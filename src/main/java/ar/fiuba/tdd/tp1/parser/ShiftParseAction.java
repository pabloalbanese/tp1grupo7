package ar.fiuba.tdd.tp1.parser;

// Represents the action of shifting in a certain token in an LR1 parser.
public class ShiftParseAction implements LR1ParseAction {

    String nextState = null;

    public ShiftParseAction(String nextState) {
        this.nextState = nextState;
    }

    @Override
    public void apply(LR1Parser parser) {
        parser.stateStack.push(this.nextState);
        ParseTree newPartialTree = new ConcreteParseTree();
        newPartialTree.setNode(parser.lookahead());
        parser.parseStack.push(newPartialTree);
        parser.parseTokens.poll();
    }

}
