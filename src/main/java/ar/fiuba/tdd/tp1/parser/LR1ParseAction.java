package ar.fiuba.tdd.tp1.parser;

public interface LR1ParseAction {

    public void apply(LR1Parser parser) throws Exception;
}
