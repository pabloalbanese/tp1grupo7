package ar.fiuba.tdd.tp1.parser;

// Interface for a parser.
public interface Parser {

    public ParseTree parse(String input) throws Exception;
}
