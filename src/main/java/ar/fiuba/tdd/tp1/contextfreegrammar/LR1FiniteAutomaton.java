package ar.fiuba.tdd.tp1.contextfreegrammar;

import java.util.HashSet;
import java.util.Set;

public class LR1FiniteAutomaton {

    Set<LR1GrammarItemSet> states;
    Set<LR1Transition>     transitions;
    LR1GrammarItemSet      startState;
    boolean                changedInLastGenerationIteration;

    public LR1FiniteAutomaton() {

        this.states = new HashSet<LR1GrammarItemSet>();
        this.transitions = new HashSet<LR1Transition>();
        this.startState = null;
    }

    public boolean addState(LR1GrammarItemSet state, boolean isStart) throws Exception {

        if (isStart && startState == null) {
            startState = state;
        } else {
            throw new Exception("Can't add new start state. Reason: start state already set.");
        }
        return this.states.add(state);
    }

    public boolean addState(LR1GrammarItemSet state) {
        try {
            return this.addState(state, false);
        } catch (Exception e) {
            // Nothing to do here, exception will never be raised.
            return false;
        }
    }

    public boolean addTransition(LR1Transition transition) {
        return this.transitions.add(transition);
    }

    public void generate() throws Exception {
        changedInLastGenerationIteration = true;
        while (changedInLastGenerationIteration) {
            changedInLastGenerationIteration = iterate();
        }

    }

    private boolean iterate() throws Exception {

        changedInLastGenerationIteration = false;
        Set<LR1GrammarItemSet> tempStates = new HashSet<LR1GrammarItemSet>();

        for (LR1GrammarItemSet state : this.states) {
            tempStates.addAll(findTransitionsFor(state));
        }

        if (this.states.addAll(tempStates)) {
            changedInLastGenerationIteration = true;
        }

        return changedInLastGenerationIteration;
    }

    private Set<LR1GrammarItemSet> findTransitionsFor(LR1GrammarItemSet state) throws Exception {

        Set<LR1GrammarItemSet> tempStates = new HashSet<LR1GrammarItemSet>();

        for (String symbol : state.getNextSymbols()) {

            LR1GrammarItemSet newState = state.gotoSymbol(symbol);
            newState = lookForExistingEquivalent(newState);
            tempStates.add(newState);

            if (this.addTransition(new LR1Transition(state, newState, symbol))) {
                changedInLastGenerationIteration = true;
            }
        }

        return tempStates;
    }

    private LR1GrammarItemSet lookForExistingEquivalent(LR1GrammarItemSet state) {
        for (LR1GrammarItemSet tempState : this.states) {
            if (tempState.equals(state)) {
                return tempState;
            }
        }
        return state;
    }
}
