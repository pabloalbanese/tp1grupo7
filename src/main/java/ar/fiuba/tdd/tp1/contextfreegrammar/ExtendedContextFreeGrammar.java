package ar.fiuba.tdd.tp1.contextfreegrammar;

import ar.fiuba.tdd.tp1.expressioninterpreter.CellParseTreeInterpreterAction;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ExtendedContextFreeGrammar {

    private static final String startRuleName  = "S' -> S$";
    private static final String newStartSymbol = "S'";
    private static final String eofSymbol      = "$";

    private Map<String, String>                         terminalSymbols;
    private Map<String, CellParseTreeInterpreterAction> nonterminalSymbols;
    private Map<String, LR1ProductionRule>              productionRules;
    private String                                      startSymbol;
    private boolean                                     locked;

    public ExtendedContextFreeGrammar() {
        this.terminalSymbols = new HashMap<>();
        this.nonterminalSymbols = new HashMap<>();
        this.productionRules = new HashMap<>();
        this.startSymbol = null;
        this.locked = false;
    }

    public void registerTerminalSymbol(String terminal, String regex) throws Exception {
        testForErrorsOnRegisterSymbol(terminal);
        this.terminalSymbols.put(terminal, regex);
    }

    public void registerNonterminalSymbol(String nonterminal, CellParseTreeInterpreterAction action) throws Exception {
        testForErrorsOnRegisterSymbol(nonterminal);
        this.nonterminalSymbols.put(nonterminal, action);
    }

    public void registerProductionRule(String name, String result, String stringRule) throws Exception {

        String[] rule = stringRule.split(" ");
        LR1ProductionRule newRule = new LR1ProductionRule(name, result, rule);
        testForErrorsOnRegisterRule(newRule);
        this.productionRules.put(newRule.name, newRule);
    }

    public void registerStartSymbol(String start) throws Exception {
        testForErrorsOnRegisterStartSymbol(start);
        this.startSymbol = start;
    }

    public void lock() throws Exception {
        // TODO: check for correct grammar. (for example, each nonterminal
        // should have at least a production rule to get to it)
        this.registerTerminalSymbol(eofSymbol, "$/%");
        this.registerNonterminalSymbol(newStartSymbol, null);
        this.registerProductionRule(startRuleName, "S'", this.startSymbol + " " + eofSymbol);
        this.locked = true;
    }

    public List<String> getTerminalSymbols() {
        List<String> terminals = new ArrayList<>(this.terminalSymbols.keySet());
        return Collections.unmodifiableList(terminals);
    }

    public List<String> getNonterminalSymbols() {
        List<String> nonterminals = new ArrayList<>(this.nonterminalSymbols.keySet());
        return Collections.unmodifiableList(nonterminals);
    }

    public List<String> getProductionRuleNames() {
        List<String> rules = new ArrayList<>(this.productionRules.keySet());
        return Collections.unmodifiableList(rules);
    }

    public String getRegexForTerminal(String terminal) {
        return this.terminalSymbols.get(terminal);
    }

    public CellParseTreeInterpreterAction getActionForNonterminal(String nonterminal) {
        return this.nonterminalSymbols.get(nonterminal);
    }

    public LR1ProductionRule getProductionRule(String ruleName) {
        return this.productionRules.get(ruleName);
    }

    public String getStartSymbol() {
        return this.startSymbol;
    }

    public boolean isSymbol(String symbol) {
        return isTerminalSymbol(symbol) || isNonterminalSymbol(symbol);
    }

    public boolean isTerminalSymbol(String symbol) {
        return this.terminalSymbols.containsKey(symbol);
    }

    public boolean isNonterminalSymbol(String symbol) {
        return this.nonterminalSymbols.containsKey(symbol);
    }

    public boolean isProductionRule(String ruleName) {
        return this.productionRules.containsKey(ruleName);
    }

    public List<LR1ProductionRule> getRulesFor(String result) {

        // TODO: consider no rules found case.
        List<LR1ProductionRule> matchingRules = new ArrayList<LR1ProductionRule>();
        for (LR1ProductionRule rule : productionRules.values()) {
            if (rule.result.equals(result)) {
                matchingRules.add(rule);
            }
        }
        return matchingRules;
    }

    private void testForLocked() throws Exception {
        if (this.locked) {
            throw new Exception("Can't add to this grammar. Reason: grammar already locked.");
        }
    }

    private void testForErrorsOnRegisterSymbol(String toRegister) throws Exception {

        testForLocked();

        if (this.nonterminalSymbols.containsKey(toRegister) || this.terminalSymbols.containsKey(toRegister)) {
            throw new Exception(
                    "Can't register new symbol. Reason: symbol already registered as a nonterminal in this grammar.");
        }
    }

    private void testForErrorsOnRegisterRule(LR1ProductionRule toRegister) throws Exception {
        testForLocked();
        testForCorrectResult(toRegister);
        testForSymbol(toRegister);
        testForDuplicate(toRegister);
    }

    private void testForCorrectResult(LR1ProductionRule toRegister) throws Exception {
        if (!nonterminalSymbols.containsKey(toRegister.result)) {
            throw new Exception("Can't register new production rule. Reason: result \"" + toRegister.result
                    + "\" is not a single nonterminal.");
        }
    }

    private void testForSymbol(LR1ProductionRule toRegister) throws Exception {
        for (String symbol : toRegister.rule) {
            if (!nonterminalSymbols.containsKey(symbol) && !terminalSymbols.containsKey(symbol)) {
                throw new Exception(
                        "Can't register new production rule. Reason: unrecognized symbol \"" + symbol + "\".");
            }
        }
    }

    private void testForDuplicate(LR1ProductionRule toRegister) throws Exception {
        if (productionRules.containsKey(toRegister.name) || productionRules.containsValue(toRegister)) {
            throw new Exception("Can't register new production rule. Reason: duplicate rule.");
        }
    }

    private void testForErrorsOnRegisterStartSymbol(String start) throws Exception {

        testForLocked();

        if (!this.nonterminalSymbols.containsKey(start)) {
            throw new Exception("Can't register as start symbol. Reason: unrecognised symbol.");
        }
        if (this.startSymbol != null) {
            throw new Exception("Can't register as start symbol. Reason: start symbol already exists.");
        }
    }

    public Set<String> calculateFirstSet(String symbol) throws Exception {

        Set<String> set = new HashSet<String>();

        if (isTerminalSymbol(symbol)) {
            set.add(symbol);
        } else if (isNonterminalSymbol(symbol)) {
            for (LR1ProductionRule rule : this.getRulesFor(symbol)) {
                set.addAll(this.calculateFirstSet(rule.getSymbol(0)));
            }
        } else {
            throw new Exception("Can't calculate FIRST set. Reason: unrecognized symbol \"" + symbol + "\".");
        }

        return set;
    }

    public LR1GrammarItem getStartItem() {
        LR1GrammarItem startItem = new LR1GrammarItem(this.productionRules.get(startRuleName), eofSymbol);
        return startItem;
    }
}
