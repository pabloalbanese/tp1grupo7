package ar.fiuba.tdd.tp1.contextfreegrammar;

public class LR1Transition {

    LR1GrammarItemSet currentState;
    LR1GrammarItemSet nextState;
    String            symbol;

    public LR1Transition(LR1GrammarItemSet currentState, LR1GrammarItemSet nextState, String symbol) {

        this.currentState = currentState;
        this.nextState = nextState;
        this.symbol = symbol;
    }

    @Override
    public int hashCode() {
        return this.symbol.hashCode();
    }

    private boolean equalsTransition(LR1Transition other) {
        return this.currentState.equals(other.currentState) && this.nextState.equals(other.nextState)
                && this.symbol.equals(other.symbol);
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj) {
            return true;
        }

        if (obj == null || !this.getClass().equals(obj.getClass())) {
            return false;
        }

        return this.equalsTransition((LR1Transition) obj);
    }

}
