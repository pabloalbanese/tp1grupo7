package ar.fiuba.tdd.tp1.contextfreegrammar;

public class LR1GrammarItem {

    LR1ProductionRule rule;
    String            lookahead;
    int               position;

    public LR1GrammarItem(LR1ProductionRule rule, String lookahead) {
        this.rule = rule;
        this.lookahead = lookahead;
        this.position = 0;
    }

    private LR1GrammarItem(LR1ProductionRule rule, String lookahead, int startPosition) {
        this.rule = rule;
        this.lookahead = lookahead;
        this.position = startPosition;
    }

    public LR1GrammarItem getNextItem() {

        LR1GrammarItem item = null;
        if (this.position < this.rule.length()) {
            item = new LR1GrammarItem(this.rule, this.lookahead, this.position + 1);
        }
        return item;
    }

    public String getNextSymbol() {

        try {
            return this.rule.getSymbol(this.position);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public int hashCode() {
        return this.lookahead.hashCode() + this.position;
    }

    private boolean equalsGrammarItem(LR1GrammarItem other) {
        return this.rule.equals(other.rule) && this.lookahead.equals(other.lookahead)
                && this.position == other.position;
    }

    @Override
    public boolean equals(Object obj) {

        if (obj == null || !this.getClass().equals(obj.getClass())) {
            return false;
        }

        if (this == obj) {
            return true;
        }

        return this.equalsGrammarItem((LR1GrammarItem) obj);
    }
}
