package ar.fiuba.tdd.tp1.contextfreegrammar;

public class LR1ParseTableBuilder {

    ExtendedContextFreeGrammar grammar;
    LR1FiniteAutomaton         automaton;
    LR1ParseTable              parseTable;

    public void generateFiniteAutomaton() throws Exception {

        this.automaton = new LR1FiniteAutomaton();

        LR1GrammarItemSet startState = buildStartState();
        automaton.addState(startState, true);
        automaton.generate();
    }

    private LR1GrammarItemSet buildStartState() throws Exception {
        LR1GrammarItemSet startState = new LR1GrammarItemSet(this.grammar);
        LR1GrammarItem item = this.grammar.getStartItem();
        startState.addLR1GrammarItem(item);
        startState.closeSet();

        return startState;
    }

    public LR1ParseTable generateLR1FSM(ExtendedContextFreeGrammar grammar) throws Exception {

        this.grammar = grammar;
        generateFiniteAutomaton();

        this.parseTable = new LR1ParseTable(this.grammar);

        registerStates();
        this.parseTable.registerStartState(this.automaton.startState.id);
        handleTransitions();
        handleStates();

        return this.parseTable;
    }

    private void registerStates() {
        for (LR1GrammarItemSet state : this.automaton.states) {
            this.parseTable.registerState(state.id);
        }
    }

    private void handleTransitions() throws Exception {
        for (LR1Transition transition : this.automaton.transitions) {

            if (grammar.isTerminalSymbol(transition.symbol)) {
                this.parseTable.registerStateTableEntry(transition.currentState.id, transition.symbol,
                        transition.nextState.id);
            } else if (grammar.isNonterminalSymbol(transition.symbol)) {
                this.parseTable.registerGotoTableEntry(transition.currentState.id, transition.symbol,
                        transition.nextState.id);
            }
        }
    }

    private void handleStates() throws Exception {

        for (LR1GrammarItemSet state : this.automaton.states) {
            for (LR1GrammarItem item : state.items) {
                if (item.equals(this.grammar.getStartItem().getNextItem())) {
                    this.parseTable.registerStateTableEntry(state.id, grammar.getStartItem().lookahead, "DONE");
                }
                if (item.getNextSymbol() == null) {
                    this.parseTable.registerStateTableEntry(state.id, item.lookahead, item.rule.name);
                }
            }
        }
    }

}
